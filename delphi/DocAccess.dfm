object Form3: TForm3
  Left = 0
  Top = 0
  HorzScrollBar.Color = clRed
  HorzScrollBar.ParentColor = False
  Caption = #1059#1095#1077#1090' '#1086#1087#1077#1088#1072#1094#1080#1081' '#1087#1086' '#1087#1088#1086#1076#1072#1078#1077' '#1088#1072#1079#1085#1099#1093' '#1090#1080#1087#1086#1074' '#1085#1077#1076#1074#1080#1078#1080#1084#1086#1089#1090#1080' '#1072#1075#1077#1085#1090#1072#1084#1080' '
  ClientHeight = 399
  ClientWidth = 687
  Color = clSilver
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clDefault
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Select: TButton
    Left = 350
    Top = 8
    Width = 179
    Height = 40
    Cursor = crHandPoint
    Hint = #1042#1099#1074#1077#1089#1090#1080' '#1074#1099#1073#1088#1072#1085#1085#1099#1077' '#1086#1087#1077#1088#1072#1094#1080#1080
    Caption = 'Enter'
    Font.Charset = ARABIC_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'Viner Hand ITC'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnClick = SelectClick
  end
  object Clear: TButton
    Left = 350
    Top = 54
    Width = 179
    Height = 40
    Cursor = crHandPoint
    Hint = #1059#1076#1072#1083#1080#1090#1100' '#1087#1072#1088#1072#1084#1077#1090#1088#1099' '#1086#1090#1073#1086#1088#1072
    Caption = 'Cleane'
    Font.Charset = ARABIC_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'Viner Hand ITC'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = ClearClick
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 160
    Width = 673
    Height = 231
    Hint = #1054#1082#1085#1086' '#1074#1099#1074#1086#1076#1072' '#1076#1072#1085#1085#1099#1093
    Color = 15395562
    DataSource = DataModule4.DataSource2
    Font.Charset = JOHAB_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 336
    Height = 133
    Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1086#1090#1073#1086#1088#1072
    Color = 15000804
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    object Label1: TLabel
      Left = 20
      Top = 22
      Width = 75
      Height = 13
      Caption = #1053#1077#1076#1074#1080#1078#1080#1084#1086#1089#1090#1100
    end
    object Label2: TLabel
      Left = 20
      Top = 65
      Width = 131
      Height = 13
      Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    end
    object Label3: TLabel
      Left = 20
      Top = 106
      Width = 31
      Height = 13
      Caption = #1057#1091#1084#1084#1072
    end
    object Label4: TLabel
      Left = 137
      Top = 106
      Width = 14
      Height = 13
      Caption = #1054#1090
    end
    object Label5: TLabel
      Left = 231
      Top = 106
      Width = 14
      Height = 13
      Caption = #1044#1086
    end
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 157
      Top = 22
      Width = 156
      Height = 21
      Cursor = crAppStart
      Hint = #1042#1099#1073#1077#1088#1080#1090#1077' '#1085#1077#1076#1074#1080#1078#1077#1084#1086#1089#1090#1100
      ListSource = DataModule4.DataSource1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object Memo1: TMemo
      Left = 157
      Top = 58
      Width = 156
      Height = 21
      Hint = #1042#1074#1077#1076#1080#1090#1077' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object Edit1: TEdit
      Left = 157
      Top = 100
      Width = 62
      Height = 21
      Hint = #1042#1074#1077#1076#1080#1090#1077' '#1084#1080#1085#1080#1084#1072#1083#1100#1085#1091#1102' '#1089#1091#1084#1084#1091' '#1076#1077#1085#1077#1075
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object Edit2: TEdit
      Left = 251
      Top = 100
      Width = 62
      Height = 21
      Hint = #1042#1074#1077#1076#1080#1090#1077' '#1084#1072#1082#1089#1080#1084#1072#1083#1100#1085#1091#1102' '#1089#1091#1084#1084#1091' '#1076#1077#1085#1077#1075
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
  end
  object Button2: TButton
    Left = 566
    Top = 137
    Width = 113
    Height = 25
    Caption = #1042#1099#1079#1086#1074' '#1089#1087#1088#1072#1074#1082#1080
    TabOrder = 4
    Visible = False
    OnClick = Button2Click
  end
  object XPManifest1: TXPManifest
    Left = 640
    Top = 184
  end
end
